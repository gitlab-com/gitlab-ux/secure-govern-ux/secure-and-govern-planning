<!--{Issue title should be Sec Team Planning Issue for xx.x (mmm 17th - mmm 17th)} 
where xx.x is the milestone this issue is being planned for and mmm is the start and end months of the milestone-->

<details>
<summary>

### Product Designer's Available Capacity

</summary>

<!--Group Points: These are the weight points you can handle for your planned group work in the current Milestone. Think of this as your committed capacity. The number of points you anticipate dedicating to the main planned tasks.
Flex Capacity: These are extra weight points you hold in reserve for any unexpected tasks that come up—like OKR-driven requests, departmental initiatives, urgent fixes, or last-minute design needs. We know surprises happen, so building in this buffer keeps us agile and protects your workload. An additional buffer of points for unplanned work.-->

| Designer | Group Capacity | Flex Capacity | TOTAL Capacity |
|----------|----------------|---------------|----------------|
| Becka |  |  |  |
| Bonnie |  |  |  |
| Camellia |  |  |  |
| Ilonah |  |  |  |
| Michael |  |  |  |
| Michael |  |  |  |

</details>

<details>
<summary>

### Product Teams's Out Of Office (OOO)

</summary>

| Designer | OOO Start - End |
|----------|-----------------|
| Becka |  |
| Bonnie |  |
| Camellia |  |
| Dean |  |
| Grant |  |
| Ian |  |
| Ilonah |  |
| Jocelyn |  |
| Joe |  |
| John |  |
| Justin |  |
| Michael F. |  |
| Sara |  |
| Torian |  |

</details>

<details>
<summary>

### CSAT Insights Issues

</summary>

<!--Our goal should be to keep these at 0, at the least we need to maintain the SLAs.-->

* [Authentication](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::authentication&first_page_size=100)
* [Authorization](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::authorization&first_page_size=100)
* [Compliance](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::compliance&first_page_size=100)
* [Composition analysis](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::composition%20analysis&first_page_size=100)
* [Dynamic Analysis](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::dynamic%20analysis&first_page_size=100)
* [Pipeline Security](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::pipeline%20security&first_page_size=100)
* [Secret Detection](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::secret%20detection&first_page_size=100)
* [Security Policies](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::security%20policies&first_page_size=100)
* [Static Analysis](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::static%20analysis&first_page_size=100)
* [Threat Insights](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Actionable%20Insight::Product%20change&label_name%5B%5D=group::threat%20insights&first_page_size=100)

#### 10 Recently Opened Insight Issues Across All Groups

```glql
---
display: table
fields: labels("group::*"), title, labels("severity::*"), updated
limit: 10
---
group = "gitlab-org" AND label = ("section::sec", "Actionable Insight::Product change") AND label in ("severity::*") AND opened = true
```

</details>

# Group Work Needs/Prioritization

<!--Each PM will add their Group's top 3 issues to the table. Issues can be for design or research. Add issues in priority order, top (higher priority) to bottom,  Then mark them as a :100: **Need* (higher priority) Vs a :muscle: **Nice To Have** (lesser priority), and finally when you anticipate needing the work done so it can be prioritized for the Build Phase.

 Each Designer will add any issue to a Group's table that they believe is necessary and important to work on during this milestone.-->

<!--As a team we'll evaluate each Group's issue priority needs and determine who has the greatest need by looking at the first Priority column's prior* As a team, we'll evaluate each Group's issue priority needs and determined who has the greatest need by looking at the Priority number, measuring that against the Need/Nice to have columns, and then finally against when it needs to be Ready for Build By. All of these items together will determine an overall priority that can be used as a measure against another issue in another Group's issue set. We'll then discuss which issues need to get picked up by which designer..-->

### **Authentication**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::authentication", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

### **Authorization**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::authorization", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

### **Compliance**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::compliance", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

### **Secret Detection**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::secret detection", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

### **Pipeline Security**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::pipeline security", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

### **Security Insights**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::security insights", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

### **Security Platform Management**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::security platform management", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

### **Security Policies**

```glql
---
display: table
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::security policies", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

# Team's Planned Milestone Work

<details>
<summary>

### Becka's Work

</summary>

**Security Insights**

```glql
---
display: list
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::security insights", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

**Non-group/additional**

```glql
---
display: list
fields: title, labels("workflow::*"), milestone, weight
limit: 10
---
group = "gitlab-org" AND label = ("UX") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND label != ("section::sec", "group::security insights", "Design Priority::*") AND milestone in ("xx.xx") AND assignee = "beckalippert"
```

</details>

<details>
<summary>

### Bonnie's Work

</summary>

**Pipeline Security**

```glql
---
display: list
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::pipeline security", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

**Non-group/additional**

```glql
---
display: list
fields: title, labels("workflow::*"), milestone, weight
limit: 10
---
group = "gitlab-org" AND label = ("UX") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND label != ("section::sec", "group::pipeline security", "Design Priority::*") AND milestone in ("xx.xx") AND assignee = "bonnie-tsang"
```

</details>

<details>
<summary>

### Camellia's Work

</summary>

**Compliance**

```glql
---
display: list
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::compliance", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

**Security Policies**

```glql
---
display: list
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::security policies", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx") AND assignee = "cam.x"
```

**Non-group/additional**

```glql
---
display: list
fields: title, labels("workflow::*"), milestone, weight
limit: 10
---
group = "gitlab-org" AND label = ("UX") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND label != ("section::sec", "group::security policies", "group::compliance" , "Design Priority::*") AND milestone in ("xx.xx")  AND assignee = "cam.x"
```

</details>

<details>
<summary>

### Ilonah's Work

</summary>

**Authorization**

```glql
---
display: list
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::authorization", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

**Non-group/additional**

```glql
---
display: list
fields: title, labels("workflow::*"), milestone, weight
limit: 10
---
group = "gitlab-org" AND label = ("UX") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND label != ("section::sec", "group::authorization", "Design Priority::*") AND milestone in ("xx.xx") AND assignee = "ipelaez1"
```

</details>

<details>
<summary>

### Michael's Work

</summary>

**Security Platform Management**

```glql
---
display: list
fields: labels("Design Priority::*"), title, labels("workflow::*"), milestone, weight, assignee
limit: 6
---
group = "gitlab-org" AND label = ("section::sec", "group::security platform management", "Design Priority::*") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND milestone in ("xx.xx")
```

**Non-group/additional**

```glql
---
display: list
fields: title, labels("workflow::*"), milestone, weight
limit: 10
---
group = "gitlab-org" AND label = ("UX") AND label in ("workflow::design", "workflow::problem validation", "workflow::solution validation") AND label != ("section::sec", "group::security platform management", "Design Priority::*") AND milestone in ("xx.xx") AND assignee = "mfangman"
```

</details>

* [ ] Set the Milestone (current Milestone)
* [ ] Set the Due Date for the end of the current Milestone
* [ ] Assign the entire team PMs and PDs
* [ ] Ensure any new research needs are put into the [Research Prioritization](https://docs.google.com/spreadsheets/d/12qckNjL7HGEwJeGwVnVUwk3fbuEj1DSB0N4paSwgAks/edit?usp=sharing) sheet

/label ~section::sec ~UX ~type::ignore